Sncf
name: Sncf

version: 0.0.1-SNAPSHOT

description: Ce programme parse un fichier log.Chaque ligne correspond � une connexion, depuis un serveur (<hostname> � gauche) vers un autre serveur (<hostname> � droite) � un instant donn�.


Les lignes sont approximativement tri�es par timestamp. L'approximation est de l'ordre de 5 minutes.
Par exemple:


1366815793 quark garak
1366815795 brunt quark
1366815811 lilac garak

Le programme doit produire � chaque heure:
* la liste des serveurs qui ont �t� connect�s � un serveur donn� durant cette heure
* la liste des serveurs auxquels un serveur donn� s'est connect� durant cette heure
* le serveur qui a g�n�r� le plus de connections durant cette heure