package com.oui.sncf.sncf;

import com.oui.sncf.sncf.batch.TraitementBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SncfApplication implements CommandLineRunner {

    @Autowired
    private TraitementBatch traitementBatch;

    public static void main(String[] args)  {
        SpringApplication.run(SncfApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        traitementBatch.genererBatch();
    }


}