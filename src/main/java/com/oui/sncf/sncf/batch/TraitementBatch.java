package com.oui.sncf.sncf.batch;

import com.oui.sncf.sncf.entity.ServeurConnection;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class TraitementBatch {

    @Value("${vers.serveurname}")
    private String ServeurCible;

    @Value("${apartir.serveurname}")
    private String ServeurApartir;


    Map<String, List<ServeurConnection>> mapByServeurApartir = new HashMap<>();
    Map<String, List<ServeurConnection>> mapByServeurCible = new HashMap<>();


    public TraitementBatch() {
    }

    @Scheduled(fixedRate=60*60*1000, initialDelay=10*60*1000)
    public void genererBatch() throws IOException {

        System.out.println(" --------------- Debut du Traitement du Batch --------------------\n");
        TraitementBatch();

    }

    /**
     * Cette Méthode permet de lire le fichier log de parser chaque ligne
     * et de créer un fichier qui va fournir les détails des serveurs
     */
    public void TraitementBatch() throws IOException {

        Stream<String> lines = Files.lines(Paths.get("src/main/log/log.txt"));

        lines.forEach(i -> {
            parseLigne(i);
        });


        try {
            String filename = "src/main/log/log-" + System.currentTimeMillis() + ".txt";

            String body = genererLogFile();

            FileOutputStream outputStream = new FileOutputStream(filename);
            byte[] strToBytes = body.getBytes();
            outputStream.write(strToBytes);

            outputStream.close();


        } catch (IOException e) {

            e.printStackTrace();

        }

    }

    /**
     * Cette Méthode permet de génerer le fichier des détails basé sur le fichier log
     */
    private String genererLogFile() {

        StringBuilder sb = new StringBuilder();
        sb.append(getServeurConnecteApartirDe());
        sb.append(getServeurConnecteCible());
        sb.append(getHautServeurConnection());

        return sb.toString();
    }


    /**
     * Cette Méthode permet de récuppérrer les noms des serveurs que le serveur de base à pu se connecter
     */
    private String getServeurConnecteApartirDe() {
        List<ServeurConnection> serveurConnections = mapByServeurApartir.get(ServeurApartir);
        List<String> serveursNames = Collections.emptyList();
        if (serveurConnections != null) {
            serveursNames = serveurConnections.stream().map(ServeurConnection::getServeurCible).distinct().collect(toList());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("\nLe serveur ");
        sb.append(ServeurApartir);
        sb.append(" a généré des connexion vers :");
        sb.append("\n");
        sb.append(String.join(",", serveursNames));
        sb.append("\n");

        return sb.toString();
    }

    /**
     * Cette Méthode permet de récuppérrer les noms des serveurs qui se sont connété à un serveur
     */

    private String getServeurConnecteCible() {
        List<ServeurConnection> serveurConnections = mapByServeurCible.get(ServeurCible);
        List<String> serveursNames = Collections.emptyList();
        if (serveurConnections != null) {
            serveursNames = serveurConnections.stream().map(ServeurConnection::getServeurApartir).distinct().collect(toList());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("\nLes serveurs qui se sont  connectés au serveur ");
        sb.append(ServeurCible);
        sb.append(" sont : ");
        sb.append("\n");
        sb.append(String.join(",", serveursNames));
        sb.append("\n");

        return sb.toString();
    }

    /**
     * Cette Méthode permet de récuppérrer le serveur qui a généré le plus haut nombre de connexion
     */
    private String getHautServeurConnection() {

        Optional<List<ServeurConnection>> connectionsApartirDe = mapByServeurApartir.values().stream().max(Comparator.comparingInt(List::size));
        Optional<List<ServeurConnection>> connectionsVers = mapByServeurCible.values().stream().max(Comparator.comparingInt(List::size));
        String ServeurLePLusSolicite = "";

        if (!connectionsApartirDe.get().isEmpty() && !connectionsVers.get().isEmpty()) {
            if (connectionsApartirDe.get().size() > connectionsVers.get().size()) {
                ServeurLePLusSolicite = connectionsApartirDe.get().get(0).getServeurApartir();
            } else if (connectionsApartirDe.get().size() < connectionsVers.get().size()) {
                ServeurLePLusSolicite = connectionsVers.get().get(0).getServeurCible();
            } else {
                StringBuilder sbc = new StringBuilder();
                sbc.append(connectionsApartirDe.get().get(0).getServeurApartir());
                sbc.append(",");
                sbc.append(connectionsVers.get().get(0).getServeurCible());
                ServeurLePLusSolicite = sbc.toString();
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("\nLe serveur qui a généré le plus de connexion dans l'heure passée est : \n");
        sb.append(ServeurLePLusSolicite);
        sb.append("\n");

        return sb.toString();


    }

    /**
     * Cette Méthode permet de parser les lignes du fichier log en entrée du batch
     */
    public Optional<ServeurConnection> parseLigne(String ligne) {
        if (ligne != null && !ligne.isEmpty()) {
            StringTokenizer st = new StringTokenizer(ligne);
            ServeurConnection serveurConnection = new ServeurConnection();

            serveurConnection.setLocalDateTime(parseTimeStamp(st.nextToken()));
            serveurConnection.setServeurApartir(st.nextToken());
            serveurConnection.setServeurCible(st.nextToken());

            MapPopulate(serveurConnection);

            return Optional.of(serveurConnection);
        }
        return Optional.empty();

    }

    /**
     * Cette Méthode permet de parser le timpeStamp basé sur le fichier log
     */
    private LocalDateTime parseTimeStamp(String timestamp) {

        long instant = Long.valueOf(timestamp);
        LocalDateTime connectionDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(instant), TimeZone.getDefault().toZoneId());

        return connectionDateTime;
    }


    /**
     * Cette Méthode permet de stocker deux map clé serveurde base /serveur cible et ces valeurs sont des list de connexionsServeurs
     */
    public void MapPopulate(ServeurConnection serveurConnection) {
        @Nullable List<ServeurConnection> connectionsFrom = mapByServeurApartir.get(serveurConnection.getServeurApartir());
        if (connectionsFrom == null) {
            connectionsFrom = new ArrayList<>();
        }
        connectionsFrom.add(serveurConnection);
        mapByServeurApartir.put(serveurConnection.getServeurApartir(), connectionsFrom);

        @Nullable List<ServeurConnection> connectionsTo = mapByServeurCible.get(serveurConnection.getServeurCible());
        if (connectionsTo == null) {
            connectionsTo = new ArrayList<>();
        }
        connectionsTo.add(serveurConnection);
        mapByServeurCible.put(serveurConnection.getServeurCible(), connectionsTo);
    }


}
