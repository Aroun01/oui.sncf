package com.oui.sncf.sncf.entity;

import java.time.LocalDateTime;
import java.util.Objects;

public class ServeurConnection {

    private LocalDateTime localDateTime;

    private String ServeurApartir;

    private String ServeurCible;

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getServeurApartir() {
        return ServeurApartir;
    }

    public void setServeurApartir(String serveurApartir) {
        ServeurApartir = serveurApartir;
    }

    public String getServeurCible() {
        return ServeurCible;
    }

    public void setServeurCible(String serveurCible) {
        ServeurCible = serveurCible;
    }


}
