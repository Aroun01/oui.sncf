package com.oui.sncf.sncf;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SncfApplicationTests {

    @Test
    void main()  {
        SncfApplication.main(new String[] {});
    }

}
